function addTitle() {
 let path = window.location.pathname.replace(/\/$/g, '');
 let titleText;
 if (path) {
  const parts = path.split('/');
  path = parts[parts.length - 1];
  titleText = path.replace(/-|_/g, ' ');
 } else {
  titleText = window.location.host;
 }
 titleText = `Table des matières de ${titleText}`;
 const h1 = document.createElement('h1');
 h1.appendChild(document.createTextNode(titleText));
 var header = document.getElementsByTagName("header")[0];
 header.insertAdjacentElement("afterend", h1);

 siteText = document.currentScript.getAttribute('title');
 titleText = `${titleText} — ${siteText}`;
 document.title = titleText;
}

function translate() {
 var elem = document.getElementsByClassName('indexhead')[0];
 elem.innerHTML = elem.innerHTML.replace('Name', 'Nom');
 elem.innerHTML = elem.innerHTML.replace('Last modified', 'Dernière modification');
 elem.innerHTML = elem.innerHTML.replace('Size', 'Taille');
 elem = document.getElementsByClassName('even')[0];
 elem.innerHTML = elem.innerHTML.replace('Parent Directory', 'Dossier parent');
}

addTitle();
translate();

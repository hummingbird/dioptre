[title]: <> (ssg5, un générateur de site statique)
[date]: <> (2020-07-03)

# ssg5, un générateur de site statique

Les générateurs de site statique deviennent de plus en plus populaires auprès des blogueurs en quête d’un site web stable et robuste. Les plus connus sont sans nul doute Jekyll et Hugo, et dans une moindre mesure Pelican. Celui que je vais présenter aujourd’hui est [ssg5](https://www.romanzolotarev.com/ssg.html).

## ssg5 en quelques lignes

ssg5 est un générateur de site statique minimaliste créé et maintenu par Roman Zolotarev, composé d'une bonne centaine de lignes de code shell, compatible POSIX, initialement écrit pour OpenBSD. C'est un outil simple, pour ne pas dire simpliste, qui a pour unique fonction de générer le code html d'une page à partir d'un fichier html ou d'un fichier markdown le cas échéant. Il n'est pas en mesure de générer un fil RSS, de générer une table des matières de billets de blog ou de gérer des thèmes. Cela doit être fait à la main ou par un autre programme utilisé en amont ou en aval de la génération du site.

Une question survient alors : pourquoi privilégier un tel programme, minimaliste en termes de fonctionnalités à des alternatives bien documentées et stables ? Pour ma part, les avantages sont les suivants : grande rapidité d'exécution, création d'un processus de déploiement continu simplifié par le très faible nombre de dépendances, possibilité de comprendre le code en quelques heures, etc. Il faut également noter qu'un tel générateur laisse une latitude appréciable pour générer un site exactement conforme aux souhaits de l'utilisateur, et c'est bien là l'essentiel. Par ailleurs, l'absence de fichier de configuration élimine une étape qui peut être douloureuse dans bien des cas.

## Contenu du répertoire source

Le répertoire source du site est habituellement appelé src. Il doit contenir deux fichiers, `_header.html` et `_footer.html`. Ces deux fichiers écrits en html formeront les parties hautes et basses du code html de chaque page. Le but est ici de créer un entête, un pied de page, de faire les appels aux différents scripts et feuilles de style nécessaires au bon affichage de la page. Un fichier `.ssgignore` peut être également ajouté pour définir un ensemble de fichiers qui seront ignorés par le programme.

Une fois cela fait, il ne reste plus qu'à créer du contenu. Il est possible de créer un fichier `index.md` qui sera la page d'accueil de notre site, et toutes les autres pages qui viendront l'enrichir.

## Génération du site, la théorie

Il est possible de se procurer le binaire sur le [site](https://www.romanzolotarev.com/bin/ssg5) de son créateur, puis une simple commande permet de générer le site :

    ssg5 src dst "Nom du site" "https://www.exemple.fr"

Le site web est ainsi généré dans le répertoire `dst`. Pour chaque fichier markdown ou html, une page web est créée. L'arborescence du répertoire `dst` est identique à celle du répertoire `src`, et que tous les autres fichiers sont directement copiés sans modification. La carte du site `sitemap.xml` est générée automatiquement. Il est donc utile de créer un fichier `robots.txt` précisant l'URL complète du site :

    user-agent: *
    sitemap: https://www.exemple.fr/sitemap.xml

On pourra se reporter à la page [robots-txt.com](http://robots-txt.com/) pour plus de précisions sur la construction de ce fichier.

## Génération du site, un exemple minimal

Essayons de produire un site minimal avec ssg5. On télécharge le programme qu'on rend exécutable.

    wget https://www.romanzolotarev.com/bin/ssg5
    chmod +x ssg5

On commence par créer le répertoire source, on crée le fichier `index.md` avec un contenu minimal, puis on génère le site.

    mkdir src
    echo '# Hello world!' > src/index.md
    ./ssg5 src dst "Nom du site" "https://www.exemple.fr"

Il ne reste plus qu'à se rendre dans le répertoire `dst` pour visualiser les fichiers ainsi créés. Pour rendre ces fichiers disponibles pour les internautes, il faudrait les téléverser sur le FTP d'un hébergeur. Un exemple détaillé des contenus des fichiers `_header.html` et `_footer.html` pourra être trouvé dans le dépôt [git](https://framagit.org/hummingbird/dioptre/) de ce site web.

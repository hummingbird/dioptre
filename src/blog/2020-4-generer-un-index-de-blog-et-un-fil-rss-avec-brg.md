[title]: <> (Générer un index de blog et un fil RSS avec brg)
[date]: <> (2020-07-17)

# Générer un index de blog et un fil RSS avec brg

## Une limite de ssg5

Une des limites de ssg5 est l'absence totale d'outil pour construire automatiquement un blog. Construire l'index des articles publiés est une action qui est pourtant absolument nécessaire et qui peut être rébarbative si le contenu du blog s'étoffe rapidement.

brg (blog and RSS generator) est un logiciel permettant de générer cet index, en regroupant la totalité des articles publiés, et de produire un fil RSS auquel les lecteurs peuvent s'abonner. La combinaison des deux logiciels, ssg5 et brg, donne alors un système autonome, capable de construire un site web statique contenant un blog.

## Philosophie de brg

brg est un script shell, compatible POSIX, qui tient en moins de 220 lignes de code. Il est adapté à mon usage, à savoir la compilation de ce site avec une machine ayant Alpine Linux comme système d'exploitation. Il pourrait fonctionner avec BSD moyennant une modification mineure concernant la syntaxe de la commande `date`.

Il s'inspire de [rssg](https://www.romanzolotarev.com/rssg.html) pour la génération du fil RSS, un utilitaire développé par Roman Zolotarev, l'auteur de ssg5.

brg est donc un logiciel simple, supposément rapide, ayant les mêmes dépendances que ssg5, à savoir Markdown.pl si les articles du blog sont écrits en markdown.

## Prérequis à l'utilisation de brg

brg s'attend à trouver dans un répertoire :

- des articles de blog au format markdown ou html dont le nom est composé de l'année de rédaction et du numéro de l'article dans cette année ;
- ces articles doivent contenir deux métadonnées : le titre de l'article et sa date de publication ;
- un fichier `_brg.md` contenant un titre qui deviendra le titre de la page d'index du blog et le titre du fil RSS ;
- ce fichier `_brg.md` doit également contenir au moins un paragraphe, ce premier paragraphe devenant la description du fil RSS.

Cet article aura alors le nom de fichier et les deux métadonnées suivantes :

    head -2 2020-4-generer-un-index-de-blog-et-un-fil-rss-avec-brg.md
    [title]: <> (Générer un index de blog et un fil RSS avec brg)
    [date]: <> (2020-07-17)

C'est le quatrième article de l'année 2020, intitulé « Générer un index de blog et un fil RSS avec brg », publié le 17 juillet 2020. Il est à noter que les deux dernières lignes sont des commentaires markdown, ils ne seront donc pas compilés lors de la création des fichiers html par ssg5. Notons également que le compilateur choisi dans le cas présent est Markdown.pl. Si le choix du compilateur change, il sera alors utile de se [renseigner](https://stackoverflow.com/questions/4823468/comments-in-markdown) sur la meilleure manière de créer un commentaire dans un fichier markdown.

Le fichier `_brg.md` contiendra, dans le cas présent, les lignes suivantes :

    # Les dernières nouvelles sur dioptre.fr

    Ici sont regroupés les derniers articles publiés sur dioptre.fr.

    Vous pouvez vous abonner au fil [RSS](http://www.dioptre.fr/blog/rss.xml).

La première ligne contient le titre qui deviendra le titre de la page d'index et le titre du fil RSS, la seconde deviendra la description du fil RSS et la troisième apparaitra uniquement dans la page d'index, comme le montrent l'[index](https://www.dioptre.fr/blog) et le [fil RSS](https://www.dioptre.fr/blog/rss.xml) de ce site.

Les sources de ce site et en particulier du blog sont [disponibles](https://framagit.org/hummingbird/dioptre/), à titre d'exemple.

## Utilisation de brg

Le script est librement téléchargeable :

    wget https://www.dioptre.fr/bin/brg

On le rend alors exécutable :

    chmod +x brg

Lors de l'exécution, la commande attend deux ou trois arguments :

- le répertoire qui contient les articles de blog et le fichier `_brg.md` ;
- l'url complète du site ;
- l'url complète du blog, où se trouvera son index.

Si l'url du site est identique à celle du blog, le troisième argument est superflu.

Pour ce site, la commande requiert les trois arguments, le site complet se trouvant dans le répertoire `src` et le blog dans le répertoire `src/blog` :

    brg src/blog 'https://www.dioptre.fr' 'https://www.dioptre.fr/blog'

Le script produira alors deux fichiers dans `src/blog`, un index `index.md` et un fichier contenant le fil RSS `rss.xml`.

Pour générer le site, on peut ensuite faire appel à ssg5, il compilera les pages statiques du site, les articles de blog et l'index de ce dernier, tout en ignorant le fichier `_brg.md`.

Pour faciliter la détection du fil RSS par les navigateurs, il est utile d'ajouter son lien au fichier `_header.html` utilisé par ssg5 pour la compilation des pages du site :

    <link rel="alternate" type="application/atom+xml" href="/blog/rss.xml">

[title]: <> (Pour une page web qui dure 10 ans ?)
[date]: <> (2020-09-04)

# Pour une page web qui dure 10 ans ?

## Un article de Jeff Huang

L'article [« Pour une page web qui dure 10 ans ? »](https://framablog.org/2020/08/24/pour-une-page-web-qui-dure-10-ans/) a été publié le 24 août 2020, c'est une traduction d'un [article](https://jeffhuang.com/designed_to_last/) publié sur le site personnel de son auteur initial, Jeff Huang. Il y présente un certain nombre d'idées, pour un web au contenu plus durable, avec un ensemble de propositions pour que ce contenu puisse survivre 10 ans au minimum.

Cet article est plus une base de discussion devant mener à une réflexion, qu'une liste de recommandations à suivre à la lettre. Je vais donc ici exposer mes réflexions en lien avec cet article.

## 1. Revenez à du html/css « Vanilla »

Composer un site web en 2020 sans javascript, c'est possible. Attention, ici, je parle bien d'un site web et pas d'une application web qui devra par définition avoir davantage de capacités qu'un simple site informationnel.

Mais est-ce que cela veut dire qu'il faut directement rédiger le site en langage html ? À titre individuel, je pense que le langage html possède une syntaxe complexe. Le [langage markdown](/blog/2020-8-langage-markdown.html) simplifie grandement l'étape de création du contenu, même si celle-ci doit contenir une étape de conversion en page html. Cette étape n'est pas limitante en termes de maintenance du code, car le résultat est une page html qu'il est possible de modifier si besoin. C'est donc une alternative tout à fait intéressante pour simplifier l'écriture des pages web.

## 2. Ne réduisez pas ce html.

Ne pas réduire le code html présente deux grands avantages :

- simplifier le processus de mise en place du site web ;
- simplifier la lecture du code par être humain.

La simplification du code est en effet une étape qui est le plus souvent superflue vu le gain effectif de concernant la taille des fichiers, dès lors que ceux-ci ne contiennent pas des quantités de commentaires. En revanche, l'argument de la lecture facilité du code est trop léger. En effet, tous les navigateurs ont aujourd'hui un inspecteur qui permet de visualiser le code html, indenté automatiquement. Ainsi un code html simplifié reste donc préférable.

## 3. Préférez maintenir une page plutôt que plusieurs.

Une unique page sera toujours plus simple à maintenir qu'une multitude de pages. C'est un principe appliqué sur ce site, en deux endroits :

- il existe une unique page de blog, récapitulant tous les articles, sans classement autre que l'ordre chronologique ;
- le fil rss est un unique fichier xml, qui reprend tous les articles et leurs contenus.

Il est cependant des cas où une multitude de pages est préférable, comme pour un blog avec différents articles, figés dans un certain état, donc sans maintenance à prévoir.

## 4. Mettez fin à toutes les formes de liaison automatique (hotlinking).

Utiliser sur son site des ressources présentes sur d'autres sites n'est généralement pas une bonne idée. En effet, si un site tombe ou s'il ferme dans préavis, la ressource que vous utilisiez devient inaccessible. C'est un risque, et une fois la ressource perdue, elle peut être difficile à récupérer.

Il faut ajouter que si vos internautes récupèrent des ressources extérieures à votre site, ils vont laisser des traces sur d'autres serveurs que le vôtre, et donc accroître les traces qu'ils laissent lors de leur navigation.

Aussi, des traqueurs comme Google Analytics sont effectivement inutiles, il n'est pas souhaitable de fournir à Google ou à une autre société l'historique des visites que vous recevez.

## 5. N'utilisez que les 13 polices de caractères adaptées au Web.

Si l'idée du site web que vous avez est bien d'informer un public, il faut rendre le site le moins distrayant et le plus lisible possible. L'idéal est donc d'utiliser des polices bien connues, idéalement déjà présentes sur l'ordinateur de l'internaute. À ce titre, ce site web applique à la lettre ce point.

## 6. Compressez vos images de manière obsessionnelle.

Si vous avez un site web statique et que vous observez la distribution de la taille des fichiers par type d'extension, vous vous apercevrez que les images pèsent très lourd par rapport au texte. Les compresser permet d'économiser de la bande passante, et d'accélérer l'affichage des pages sur le navigateur de l'internaute.

Les formats recommandés sont jpg pour les vraies photographies, svg pour les dessins qui vectoriels et png pour les images ayant de grands aplats de couleur uniformes. Il n'empêche que faire quelques tests permet d'ajuster sa politique de compression des images.

Jeff Huang soulève malgré tout un point intéressant : si en Europe nous sommes habitués à avoir des forfaits illimités en termes de données mobiles ou fixes, il est des parties du monde, comme les États-Unis, où chaque mégaoctet coûte à l'utilisateur final. C'est donc un point très important à prendre en compte.

## 7. Éliminez le risque de rupture d'URL.

Si on souhaite garder un site web en vie pendant une longue période, il est en effet prudent de mettre en place un système permettant de contrôler l'accessibilité du site régulièrement. On peut alors utiliser un service externe, un serveur ou un petit script qui s'exécute régulièrement sur la machine du mainteneur du site.

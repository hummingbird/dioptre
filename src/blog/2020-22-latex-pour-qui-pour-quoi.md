[title]: <> (LaTeX, pour qui, pour quoi)
[date]: <> (2020-11-20)

# LaTeX, pour qui, pour quoi

Cet article est le premier d'une série qui portera sur le langage LaTeX, sur les bénéfices et inconvénients de cette solution d'édition de documents et sur sa mise en œuvre pratique.

## TeX, LaTeX, c'est quoi ?

[TeX](https://fr.wikipedia.org/wiki/TeX) est un langage de programmation développé à la fin des années 70 par [Donald Knuth](https://fr.wikipedia.org/wiki/Donald_Knuth), en réaction à la qualité discutable des productions des logiciels d'édition de l'époque. [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) est développé par [Leslie Lamport](https://fr.wikipedia.org/wiki/Leslie_Lamport) au début des années 80 comme un ensemble de macros pour le langage TeX, permettant une édition plus aisée des documents sous ce langage.

Dans la suite de ces articles, on ne parlera que LaTeX, mais nous pourrons parfois utiliser quelques lignes de TeX si le besoin s'en fait sentir.

## LaTeX, une qualité d'édition indéniable

Le langage LaTeX permet une séparation assez nette entre les étapes de construction du fond du document et celles de la forme de celui-ci. Cela est visible dès qu'on a vu de quoi était composé un fichier tex. Il contient un préambule et un corps de document. Le préambule contient toutes les informations nécessaires à la bonne mise en page et à la bonne compréhension du document par le logiciel, et le corps de document contiendra un ensemble de données qui seront présentes dans les documents final.

Contrairement à des logiciels [WYSIWYG](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_get) où le document est en train d'être créé sous nos yeux, une étape de compilation est nécessaire pour interpréter le fichier tex et le transformer en un document final, distribuable le plus souvent sous la forme d'un fichier pdf. Il faudra donc faire appel à un compilateur de fichier TeX. En 2020, trois compilateurs sont sur le devant de la scène, pdflatex, LuaTeX et XeTeX. Ces trois logiciels, pas totalement compatibles entre-eux, permettent d'obtenir un fichier pdf final à partir d'un fichier tex.

## LaTeX, pour qui ?

Apprendre le langage LaTeX est un investissement en temps significatif, et demande une pratique régulière. Il est le plus utilisé dans les milieux académiques et d'enseignement, un milieu où la qualité des documents compte, et où, ceux-ci étant complexes, laisser le soin de la mise en forme à un logiciel est un réel avantage.

À mon sens, l'investissement en vaut la peine si on se trouve dans une situation où on doit maintenir sur le long terme un ensemble de documents ayant une charte graphique commune et susceptible d'évoluer, si on doit éditer des documents scientifiques, en particulier dans les domaines des mathématiques ou de la physique, ou si on doit créer un long document, comme une thèse de doctorat ou de master. Il peut y avoir d'autres cas que ceux proposés, mais ceux-ci sont probablement plus anecdotiques.

Connaitre un langage de programmation classique et avoir des notions en base en typographie est assurément un plus dans l'apprentissage de ce langage.

## LaTeX, pour quoi ?

L'imposante communauté d'utilisateurs du langage LaTeX lui permet d'avoir un grand nombre de modules, appelés packages, qui lui permettent de s'adapter à toutes les situations ou presque. Outre les usuels mémoires ou articles scientifiques, le langage LaTeX permet l'édition de CV, de lettres ou de diaporamas, avec une excellente qualité de mise en page.

LaTeX étant versatile, il peut être utilisé pour éditer tout type de document, mais il n'est pas conseillé de l'utiliser pour un document où le fond est bien moins important que la forme en termes d'efforts investis. En effet, la personnalisation avancée d'une mise en page en LaTeX est assez difficile, celle-ci se faisant en lignes de commande.

Dans un prochain article, nous verrons de quoi doit être constitué un [document tex minimal](/blog/2020-23-creation-d-un-fichier-tex-minimal.html) puis comment [installer LaTeX sur un ordinateur](/blog/2020-24-installation-d-une-distribution-latex.html).

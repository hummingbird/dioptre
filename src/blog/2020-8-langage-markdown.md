[title]: <> (Langage markdown)
[date]: <> (2020-08-14)

# Langage markdown

## Premières considérations

Le langage markdown a été créé en 2004 par John Gruber. Il a pour but premier de rendre l'édition des pages web plus simple, en remplaçant la très lourde syntaxe html par celle plus légère de markdown.

C'est un langage de balisage léger, qui n'a pas connu d'évolution depuis sa création, et qui n'a jamais été standardisé. Aujourd'hui, un grand nombre de syntaxes coexistent et la génération d'une page web à l'aide d'un compilateur markdown peut donner un résultat surprenant s'il n'a pas été prévu pour la syntaxe employée lors de la rédaction du fichier markdown. C'est aujourd'hui un véritable problème, sans qu'une syntaxe particulière prenne le pas sur les autres à l'heure actuelle. Signalons tout de même l'initiative [CommonMark](https://commonmark.org/) qui vise à proposer une syntaxe standardisée, qui est assez largement adoptée aujourd'hui.

Cet article s'appuie sur la version vanilla de markdown, telle que décrite par Gruber sur son [site](https://daringfireball.net/projects/markdown/), mais tout ce qui est décrit ici s'applique également à la syntaxe CommonMark.

## Code de la cheatsheet markdown

On dresse alors une cheatsheet minimale, qui sera compilée ci-après.

    # Premier titre

    ## Deuxième niveau de titre

    Un paragraphe est précédé d'une ligne vide.
    Il est possible d'écrire en _italique_, en **gras** et avec une police `monospace` et d'ajouter des [liens](https://www.dioptre.fr/).

        Bloc de code
        print("Hello world!")

    Une liste est faite avec la syntaxe :

    - premier objet ;
    - deuxième objet,

    et une liste ordonnée se construit ainsi :

    1. premier objet ;
    1. deuxième objet.

    Une image peut être insérée dans la page par une ligne de code html:

    <img src="/img/2-markdown-mark.svg" alt="Texte alternatif" title="Logo Markdown" class="img-responsive" />

    [commentaire]: <> (Ce texte ne sera pas affiché !)

## Rendu de la cheatsheet markdown

On a alors le résultat :

# Premier titre

## Deuxième niveau de titre

Un paragraphe est précédé d'une ligne vide.
Il est possible d'écrire en _italique_, en **gras** et avec une police `monospace` et d'ajouter des [liens](https://www.dioptre.fr/).

    Bloc de code
    print("Hello world!")

Une liste est faite avec la syntaxe :

- premier objet ;
- deuxième objet,

et une liste ordonnée se construit ainsi :

1. premier objet ;
1. deuxième objet.

Une image peut être insérée dans la page par une ligne de code html:

<img src="/img/2-markdown-mark.svg" alt="Texte alternatif" title="Logo Markdown" class="img-responsive" />

[commentaire]: <> (Ce texte ne sera pas affiché !)

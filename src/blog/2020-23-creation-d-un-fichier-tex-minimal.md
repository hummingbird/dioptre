[title]: <> (Création d'un fichier tex minimal)
[date]: <> (2020-11-27)

# Création d'un fichier tex minimal

## Sans plus attendre, l'exemple

Voici un exemple de fichier LaTeX relativement minimal :

    \documentclass[french,12pt]{article}

    \usepackage{lipsum}
    \usepackage{babel}
    \usepackage[T1]{fontenc}

    \title{Tests avec \LaTeX}
    \author{Test~\textsc{Auteur}}

    \begin{document}

    \maketitle

    \section{Première partie}

    \subsection{Première sous-partie}

    \lipsum[1]

    \subsection{Deuxième sous-partie}

    \lipsum[2]

    \section{Deuxième partie}

    \subsection{Troisième sous-partie}

    \lipsum[3]

    \subsection{Quatrième sous-partie}

    \lipsum[4]

    \end{document}

Nous lui donnerons le nom `minimal.tex`, et prendrons soin de vérifier que son codage de caractère est bien [UTF-8](https://fr.wikipedia.org/wiki/UTF-8).

## L'anatomie d'un fichier tex

Un fichier tex est composé de plusieurs parties. Nous distinguons ce qui se trouve avant et ce qui se trouve après la commande `\begin{document}`.

### Préambule

La partie avant la commande `\begin{document}` constitue le préambule du document, il doit contenir certaines informations, comme en particulier le type de document utilisé ici.

Le type de document est donné par la commande

    \documentclass[french,12pt]{article}

La classe (le type de document) article est un bon choix de départ, simple à utiliser et versatile. Entre les crochets sont donnés deux arguments qui seront appliqués à l'ensemble du document :

- `french` : cela indique la langue du document et commande à LaTeX la production d'une typographie adaptée ;
- `12pt` : on fixe ainsi la taille de la police pour le texte usuel, les tailles des titres et sous-titres seront ajustées automatiquement en fonction de cette valeur.

On a ensuite un ensemble de packages, de modules, qui sont chargés avant la compilation à proprement parler du document. Nous chargeons :

- [lipsum](https://ctan.org/pkg/lipsum) : un package qui permet de remplir un document avec un texte issu du site [lipsum.com](https://lipsum.com/) ;
- [babel](https://ctan.org/pkg/babel) : un module qui assure la bonne mise en forme du document et d'une typographie en accord avec les règles d'usage de la langue indiquée ;
- [fontenc](https://ctan.org/pkg/fontenc) avec l'argument T1 : obligatoire pour des documents en français, ce package autorise LaTeX à choisir le bon type de police en lien avec une langue.

Enfin, et même si ce n'est pas obligatoire, on définit le titre du document et l'auteur de celui-ci. De la même manière, il serait possible de définir la date à l'aide de la commande du même nom.

### Document

Le document se trouve entre les balises `\begin{document}` et `\end{document}`. On y voit apparaitre, successivement, les commandes suivantes :

- `\maketitle` fait apparaitre le titre accompagné du nom de l'auteur et de la date de compilation du document ;
- `\section` et `\subsection` codent les titres des parties et des sous-parties du document ;
- `\lipsum` permettent de remplir le document avec du texte.

Nous nous servirons de ce document pour tester l'installation de LaTeX que nous ferons dans le [prochain article](/blog/2021-2-installation-d-une-distribution-latex.html).

[title]: <> (Cuisine pour les débutants)
[date]: <> (2020-09-25)

# Cuisine pour les débutants

Cuisiner quand on est débutant est assez difficile, surtout quand personne ne nous a appris les bases de cette discipline pourtant essentielle à notre survie et à notre bien-être. Si on veut changer des pâtes au beurre et leurs déclinaisons, il est important de trouver des ressources permettant de bien manger, tout en assurant l'apprentissage des bons gestes et la montée en compétence du cuisinier débutant.

## Guide de cuisine de l'Étudiant

Un bon livre pour débuter, Guide de cuisine de l'Étudiant de Véronique Liegeois chez Solar ([EAN : 9782263044113](https://www.lisez.com/livre-grand-format/guide-de-cuisinede-letudiant/9782263044113)), permet de se familiariser avec les techniques élémentaires de la cuisine quotidienne. Il regroupe en particulier un ensemble de recettes pour une et deux personnes, faciles et rapides à faire, et pour tout dire, impossibles à rater. Un petit regret qui est également un avantage, le livre n'est pas illustré.

## Petit Larousse cuisine facile

Le Petit Larousse cuisine facile chez Larousse ([EAN : 9782035906519](https://cuisine.larousse.fr/livre/petit-larousse-cuisine-facile-collector-9782035906519)) est un beau livre, bien illustré et très complet, qui permet de monter en compétence et d'acquérir un bon nombre des techniques de bases de la cuisine française. Les recettes ne sont pas difficiles à réaliser, mais les quantités sont assez importantes, c'est un livre à recommander aux foyers composés de deux personnes ou plus. À noter : la blanquette de veau est excellente.

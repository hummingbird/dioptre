[title]: <> (Le scripting comme mode de vie)
[date]: <> (2020-10-23)

# Le scripting comme mode de vie

Le scripting, anglicisme pour le fait de créer ou d'utiliser des scripts, peut être vu comme un mode de vie. C'est en effet une activité assez mal connue par le grand public. Posons-nous la question suivante : quel est le pourcentage des utilisateurs de l'outil informatique qui ont intégré que cet outil, ordinateur ou smartphone, était un formidable outil à exécuter des instructions, de manière itérative ou de manière récursive ? C'est bien entendu une question rhétorique, et à titre personnel, je pense que c'est là une des défaillances dans nos usages modernes de l'informatique.

## Le scripting, pour quoi faire ?

Le scripting peut avoir différents objectifs :

- automatiser une tâche rébarbative à répéter un grand nombre de fois ;
- automatiser une tâche demandant un temps d'exécution élevé qui ne nécessite pas d'interaction avec un utilisateur humain ;
- automatiser une tâche à faire à intervalle régulier, etc.

Les situations pour lesquelles il est possible de créer un script sont donc variées, mais il s'agit avant tout de simplifier la vie de l'utilisateur et d'assurer le bon déroulement de tâches routinières. On peut citer, à titre d'exemple :

- la sauvegarde de documents sur un disque dur externe ou un serveur distant ;
- la construction de documents dont la structure dépend d'éléments externes ;
- l'automatisation d'un processus qui nécessite un nombre certain d'étapes ne requérant pas une intervention externe.

## Quel langage choisir ?

La question du langage de programmation à utiliser est souvent posée. On a, au choix, deux types de réponses : la réponse dogmatique ou la réponse pragmatique.

La réponse dogmatique dira que tel ou tel langage est meilleur pour telles ou telles raisons, ces raisons dépendant de l'interlocuteur avec qui on échange. Dans la pratique, cette position permet assez souvent de remettre en perspective les choix qui ont été fait, et incitent donc, parfois, à s'intéresser à des alternatives inconnues ou délaissées.

La réponse pragmatique dépend davantage de la situation dans laquelle se trouve l'utilisateur soucieux d'automatiser un ensemble d'actions. Cette réponse est probablement la plus intéressante pour les raisons suivantes :

- un utilisateur averti sait que tel ou tel langage de programmation est plus efficace pour une situation donnée, mais ce n'est malheureusement pas l'unique facteur à prendre en compte ;
- également, la qualité du code fourni ou les capacités du langage choisi ne sont pas les paramètres essentiels du choix ;
- le plus souvent, le besoin d'un script se fait sentir lors de l'exécution d'une tâche, et l'idéal est de programmer le script dans l'instant pour en bénéficier le plus vite possible.

Ainsi, le script intervient donc pour répondre à un besoin, à un instant donné et dans un certain contexte. Autant tenir compte de ces faits pour les choix futurs.

## Mais dans la pratique, que choisir ?

La réponse à cette question dépend bien entendu au contexte. Il est un point essentiel : quelles sont les capacités, temporelles, techniques, matérielles dont vous disposez ?

En effet, les ressources à mobiliser pour écrire un programme en C, en bash ou en python ne seront pas les mêmes. Dans la pratique, les questions fondamentales sont les suivantes :

- quels sont les langages de programmation que vous maitrisez, ou à défaut, dont vous connaissez la syntaxe de base ?
- quel est votre objectif ?

En étant capable de répondre à ces questions, il est possible de fixer les limites de l'univers dans lequel le script sera développé.

Si nous prenons un exemple et l'univers dans lequel je travaille, j'ai deux réponses en fonction des contraintes liées à mon objectif. J'ai des compétences minimales dans deux langages de programmation, python et le shell POSIX. Dépendamment de mon objectif, je vais être amené à utiliser mon script soit dans un environnement GNU/Linux minimal, soit sur ma machine personnelle, soit sur un ensemble de postes informatiques qui ont des systèmes d'exploitation variés. Dans le premier cas, je choisis le shell POSIX, et dans les deux autres, cela dépend de la complexité du script à écrire : si le script est composé d'une suite de commandes Unix, je vais privilégier le shell, mais s'il y a des opérations logiques complexes ou des calculs à mener, je vais privilégier python. Je ne choisis donc pas le langage de programmation en fonction des performances voulues ou d'un autre critère, ce qui peut sembler contre intuitif.

## Mais quid des performances ?

Si vous suivez ce blog, vous devez avoir compris que je ne suis pas pour autant en faveur des scripts lents et lourds, et que je recherche un bon niveau de performance. En effet, un script lent aura tendance à laisser penser que l'ordinateur est lent, et rien n'est plus frustrant que d'attendre que son ordinateur finisse une tâche.

Pour autant, l'objectif principal du scripting est de répondre à un besoin initial dans un temps qui est le plus souvent limité. Apprendre un nouveau langage est un investissement important, j'aurai donc toujours tendance à conseiller d'utiliser un langage déjà connu voire maitrisé.

## La méthode quick and dirty

Ensuite, la méthode [quick and dirty](https://fr.wikipedia.org/wiki/Quick-and-dirty) présente des avantages : on obtient un résultat rapide, c'est valorisant et cela permet de rentabiliser immédiatement l'investissement fourni lors de l'écriture du programme. Cela permet également d'étrenner le programme sur une situation réelle de terrain et permet de faire un premier débogage. En revanche, il est indispensable de le reprendre, lorsque le rythme de travail devient plus apaisé, de manière à s'assurer de l'absence de bugs étranges et effectuer un nettoyage du code, pour le rendre plus lisible pour des corrections ou améliorations ultérieures.

Il est, à mon sens, peu pertinent de ne pas prendre le temps d'éclaircir son code, car un script qui ne peut pas être compris et donc impossible à maintenir perd un peu de son intérêt, ce qui serait dommage.

## Approche pragmatique et rapidité d'exécution

En définitive, seul l'accomplissement de l'objectif compte. Il faut donc bien déterminer son besoin, le plus souvent visible dès qu'un agacement face à un ordinateur se fait sentir, et également réfléchir à son contexte, de manière à y arriver de la manière la plus rapide possible. En revanche, se donner les moyens de reprendre le travail déjà effectué est une excellente chose à faire dès que possible par des revues de code et un suivi des modifications.

Et pour la question des performances, rares sont les applications qui nécessitent véritablement un temps d'exécution aussi faible que possible. Si tel est malgré tout le cas, rien n'empêche de se tourner vers un langage de bas niveau ou des méthodes d'optimisation avancées, mais le temps nécessaire à leur maitrise doit être mis dans la balance coût-bénéfice, afin d'avoir un aperçu objectif de la situation.

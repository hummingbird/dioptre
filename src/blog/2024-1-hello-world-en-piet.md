[title]: <> (Hello world en Piet)
[date]: <> (2024-11-11)

# Hello world en Piet

Le programme [Hello world](https://fr.wikipedia.org/wiki/Hello_world) est souvent un des premiers programmes que l'on code lors de l'apprentissage d'un langage de programmation. Ici, nous allons recoder le programme en Piet qui est proposé sur la [page Wikipédia](https://fr.wikipedia.org/wiki/Piet) de ce langage de programmation peu commun.

## Un mot sur Piet

Ce langage de programmation est basé sur l'exploitation d'une image colorée, inspirée des travaux de [Piet Mondrian](https://fr.wikipedia.org/wiki/Piet_Mondrian). C'est un langage exotique du fait de l'absence d'intérêt industriel à exploiter un programme codé à l'aide d'un langage interprété et qui, vu que c'est une image, est particulièrement lourd.

## Hello world en Piet

Il existe plusieurs formes du programme Hello world en Piet, dont une proposée par Thomas Schoch décrite dans un [article](https://retas.de/thomas/computer/programs/useless/piet/explain.html) très détaillé. C'est une grille 13x13, avec chaque tuile possédant une largeur de 11 pixels et surtout très esthétique.

Cette image, un gif de 16 ko, est lourde, faiblement résolue et le format choisi pour l'enregistrement produit une déformation des couleurs. Dans cet article, nous allons nous pencher sur la création d'une nouvelle image qui n'aurait pas ces défauts.

## Le format choisi

Pour ne plus avoir les défauts de la précédente image, le format devra soit être vectoriel, à savoir [svg](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics), soit permettre de beaux aplats de couleur, à savoir [png](https://fr.wikipedia.org/wiki/Portable_Network_Graphics). Nous allons donc créer une image vectorielle que nous convertirons en fichier png.

## Réemploi des données de la précédente image.

L'analyse nécessaire de la précédente image pourrait être faite à la main, mais le caractère fastidieux de la tâche encourage à employer un [interpréteur](https://piet-editor.github.io/) qui nous renseignera sur les couleurs de chaque tuile. Le tout est placé sous la forme d'une liste de liste dans un script python.


    tile_color_names = [
        ["yellow","light blue","blue","dark blue","dark magenta","dark cyan","dark yellow","dark green","cyan","yellow","magenta","dark magenta","light green"],
        ["light red","dark blue","cyan","dark cyan","light cyan","light blue","blue","magenta","dark magenta","magenta","dark magenta","light green","light red"],
        ["dark cyan","yellow","green","red","light yellow","light magenta","light red","light blue","blue","dark blue","light yellow","light red","dark yellow"],
        ["light cyan","light yellow","light magenta","dark green","yellow","light red","light blue","dark cyan","light cyan","red","yellow","light blue","dark green"],
        ["dark green","green","dark magenta","magenta","light magenta","black","black","black","light magenta","dark red","green","blue","yellow"],
        ["light blue","blue","dark yellow","light magenta","black","black","light green","black","black","light red","red","dark blue","dark yellow"],
        ["light yellow","red","dark red","light blue","black","light green","light green","light green","black","light yellow","yellow","dark magenta","light yellow"],
        ["light magenta","green","dark green","magenta","black","black","cyan","black","black","yellow","magenta","light cyan","light green"],
        ["light cyan","yellow","dark blue","dark red","light yellow","black","dark blue","black","light yellow","dark yellow","cyan","light blue","light cyan"],
        ["cyan","dark red","dark cyan","dark magenta","light magenta","magenta","dark green","magenta","light magenta","light blue","dark cyan","light green","light yellow"],
        ["light blue","dark magenta","light cyan","light blue","dark yellow","yellow","dark green","light cyan","light green","light yellow","light red","green","light magenta"],
        ["yellow","dark yellow","dark cyan","red","light red","red","light red","dark red","red","green","dark cyan","dark magenta","magenta"],
        ["cyan","dark cyan","red","light red","light magenta","dark magenta","magenta","yellow","dark green","dark yellow","yellow","cyan","dark green"],
    ]

## Les couleurs de Piet

Le langage de programmation Piet utilise des couleurs très précises, recensées sur la [page Wikipédia](https://fr.wikipedia.org/wiki/Piet) de Piet, qui seront donc placées dans un dictionnaire python.

    piet_colors = {
        "black": "#000000",
        "white": "#FFFFFF",
        "light red": "#FFC0C0",
        "light yellow": "#FFFFC0",
        "light green": "#C0FFC0",
        "light cyan": "#C0FFFF",
        "light blue": "#C0C0FF",
        "light magenta": "#FFC0FF",
        "red": "#FF0000",
        "yellow": "#FFFF00",
        "green": "#00FF00",
        "cyan": "#00FFFF",
        "blue": "#0000FF",
        "magenta": "#FF00FF",
        "dark red": "#C00000",
        "dark yellow": "#C0C000",
        "dark green": "#00C000",
        "dark cyan": "#00C0C0",
        "dark blue": "#0000C0",
        "dark magenta": "#C000C0",
    }

## Création du fichier svg

Il ne reste plus qu'à créer un fichier svg, à l'aide d'une fonction de traduction des couleurs en code couleur.

    def color_name_to_hex(color_name, piet_colors):
        return piet_colors[color_name]

Et à l'aide d'une boucle for, il n'y a plus qu'un remplissage du fichier svg à faire, avec des tuiles de 300 pixels de large.

    # Dimensions of each tile
    square_size = 300
    
    # SVG beginning
    svg_content = f'<svg width="{13 * square_size}" height="{13 * square_size}" xmlns="http://www.w3.org/2000/svg">\n'
    
    for row in range(13):
        for col in range(13):
            color = color_name_to_hex(tile_color_names[row][col], piet_colors)
            x = col * square_size
            y = row * square_size
            svg_content += f'  <rect x="{x}" y="{y}" width="{square_size}" height="{square_size}" fill="{color}" />\n'
    
    # SVG ending
    svg_content += '</svg>'
    
    # SVG to a file
    svg_filename = "output.svg"
    with open(svg_filename, "w") as file:
        file.write(svg_content)

<img src="/img/13-hello-world-piet.svg" alt="hello world en Piet" class="img-responsive">

Le fichier obtenu est vectoriel et donc de résolution infinie, et ne pèse que 11 ko, le contrat est rempli.

## Et l'image au format png ?

Nous laisserons le soin au lecteur de télécharger le fichier précédent et de le convertir en utilisant, par exemple, la commande ci-dessous qui fait appel à Inkscape.

    inkscape --export-type=png --export-filename={13-hello-world-piet.svg}"
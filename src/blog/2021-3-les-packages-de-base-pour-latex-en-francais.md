[title]: <> (Les packages de base pour LaTeX en français)
[date]: <> (2021-01-15)

# Les packages de base pour LaTeX en français

Comme nous l'avons vu dans l'[article](/blog/2020-23-creation-d-un-fichier-tex-minimal.html) présentant un exemple minimal pour tester notre installation LaTeX, il est utile de charger quelques bibliothèques spécifiques.

Nous pouvons citer, au premier rang :

- [babel](https://ctan.org/pkg/babel), pour prendre en compte les spécificités de la typographie dans une langue donnée ;
- [fontenc](https://ctan.org/pkg/fontenc) avec l'argument T1, pour assurer que la police de caractères est la bonne, avec des ligatures correctes.

Au deuxième rang, si le package tikz est utilisé, il faudra placer dans le préambule du fichier la ligne suivante :

    \usetikzlibrary{babel}

Enfin, et au troisième rang, il est possible de faire un ensemble de modification à la mise en page du fichier, de manière à le rendre plus agréable à l'œil, comme :

- changer la police de caractères pour une plus légère que celle fournie par défaut avec [lmodern](https://www.ctan.org/pkg/lm) ;
- assurer une bonne mise en forme des entêtes et des pieds de pages avec [fancyhdr](https://ctan.org/pkg/fancyhdr) ;
- prendre en charge les références et les urls avec [hyperref](https://ctan.org/pkg/hyperref).

[title]: <> (Installation d'une distribution LaTeX)
[date]: <> (2021-01-08)

# Installation d'une distribution LaTeX

## LaTeX sous Windows

La distribution phare sous Windows est [MiKTeX](https://miktex.org/). Elle est connue pour une chose : être capable de télécharger à la volée des bibliothèques manquantes pour la compilation d'un document. Pour le reste, elle est complète, et est mise à jour très régulièrement.

On a le choix de l'installation, locale ou globale sur le système, du répertoire d'installation et des paramètres format de papier.

Un ensemble d'éditeurs sont disponibles, on laissera le choix au lecteur de les découvrir.

## LaTeX sous GNU/Linux, version packagée

Il est possible d'installer une distribution LaTeX sur toutes les machines GNU/Linux, la distribution [TeX Live](https://www.tug.org/texlive/), créée par le groupe des utilisateurs de TeX ([TUG](https://www.tug.org/)). Il est possible de l'installer également sur Windows, comme MiKTeX peut-être installée sur GNU/Linux, mais ces choix sont plus anecdotiques. Sur un système dérivé de Debian, on utilisera la commande suivante :

    sudo apt install texlive texlive-lang-french

pour une installation minimale, ou bien, pour une installation complète :

    sudo apt install texlive-full

Dans ce dernier cas, l'installation est relativement longue et consommatrice d'espace disque, de l'ordre de 5 Go. C'est malgré tout la solution qui est habituellement préconisée, vu qu'elle permet de télécharger l'intégralité des bibliothèques une fois pour toutes.

## LaTeX sous GNU/Linux, installation manuelle

L'installation avec un package est sensible aux mises à jour des packages de votre distribution GNU/Linux. Cela n'est généralement pas un problème pour des [rolling release](https://fr.wikipedia.org/wiki/Rolling_release), mais pour des distributions figées avec numéro de version, on peut avoir une version de TeX Live datée, et incompatible avec les autres utilisateurs. C'est le cas en particulier pour les utilisateurs de Debian et des versions LTS d'Ubuntu.

Dans ces cas, on pourra privilégier une installation manuelle de TeX Live, qui peut être faite pour le seul utilisateur ou pour le système entier. Nous choisirons ici d'installer la dernière version de TeX Live (2020 à la date d'écriture de l'article) dans le répertoire de l'utilisateur, et on suivra le [guide officiel](https://www.tug.org/texlive/quickinstall.html) disponible en anglais, qui comprend les points suivants :

1. Télécharger l'[installeur](https://www.tug.org/texlive/acquire-netinstall.html) en veillant à bien choisir `install-tl-unx.tar.gz` ;
1. Supprimer toute trace d'une précédente installation de TeX Live, mais nous considérons ici que TeX Live n'a jamais été installé auparavant ;
1. Extraire le contenu de l'archive là où l'archive a été téléchargée :

        tar -xf install-tl-unx.tar.gz

1. Exécuter l'installeur :

        cd install-tl-YYYYMMDD/
        perl install-tl

1. Sélectionner `O` puis `P` pour définir le format du papier par défaut comme étant A4, puis `R` pour revenir au menu ;
1. Sélectionner `D` puis `1` pour définir le chemin d'installation, nous considérerons ici :

    ~/textlive/2020

1. Sélectionner `I` pour installer la distribution, ce qui prendra 7 Go environ.
1. Ajouter à la fin du fichier `~/.profile` la ligne suivante, qui permettra au système de trouver les exécutables de la distribution LaTeX :

    PATH=$HOME/texlive/2020/bin/x86_64-linux:$PATH

L'installation est complète, tous les packages disponibles sont téléchargés.

## Test de la distribution

Une fois la distribution installée, il s'agit de la tester. Vérifions en premier lieu que la version installée et la bonne :

    tex --version

Cette commande devrait retourner un numéro de version faisant apparaitre la version de TeX Live disponible.

Ensuite, nous pouvons reprendre l'exemple introduit dans un [précédent article](https://www.dioptre.fr/blog/2020-23-creation-d-un-fichier-tex-minimal.html), l'enregistrer avec le nom `minimal.tex`. Sous GNU/Linux, nous pourrons alors le compiler avec la commande :

    pdflatex minimal.tex

Nous verrons apparaitre un ensemble de fichiers relatifs à la compilation et un fichier pdf (idéalement non vide), qui contient les informations souhaitées. Félicitations, vous avez compilé votre premier fichier TeX. La sortie devrait tenir sur deux pages, comme le montre le montage ci-dessous.

<img src="/img/12-minimal.png" alt="pdf de minimal.tex" class="img-responsive">

Sous Windows, il vous faudra ouvrir le fichier ainsi créé avec un éditeur LaTeX que vous aurez découvert, et vous devriez obtenir sensiblement le même résultat.

## Mise à jour de TeX Live

Même si cela n'est pas utile (et parfois même déconseillé), il est possible de la [mettre à jour](https://www.tug.org/texlive/pkginstall.html). Pour cela, on liste dans un premier temps les packages qui doivent être mis à jour, puis si on est satisfait, on peut procéder à la mise à jour :

    tlmgr update --list
    tlmgr update --self && tlmgr update --all

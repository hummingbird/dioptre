[title]: <> (Les scripts et la norme POSIX)
[date]: <> (2020-07-10)

# Les scripts et la norme POSIX

## Un petit point historique

La famille des systèmes Unix est grande, mais ce n'est rien comparé à sa taille dans les années 80. À cette époque, de grandes sociétés se font face et proposent chacune une version d'Unix, avec leurs spécificités. Cela a entrainé une perte de la portabilité des applications d'une plateforme à une autre.

Une norme fut alors proposée par l'IEEE en 1988, la norme POSIX pour Portable Operating System Interface. Elle avait donc pour objectif de normaliser les comportements des briques essentielles de chaque système comme le shell, permettant ainsi de retrouver une forme de portabilité des applications. Les différents systèmes pouvaient et peuvent toujours être certifiés compatibles POSIX.

Cependant, cette certification coutant cher et la documentation liée à ce standard n'étant pas publique, une autre norme a émergé, la norme SUS Single Unix Specification. Aujourd'hui, c'est la version 3 de cette norme qui est en vigueur, et elle présente les grands avantages d'avoir intégré le contenu de la norme POSIX et d'être disponible librement.

Il est donc intéressant, aussi souvent que possible, d'écrire des scripts shells compatibles avec la norme POSIX, de manière à pouvoir les partager largement.

## Shells et systèmes d'exploitation

Aujourd'hui, les shells les plus courants sont bash, zsh, fish, ksh, etc. Ils sont nombreux, et une grande partie d'entre eux sont dits compatibles POSIX. Tous ont leurs spécificités, ils pourront alors exécuter des applications qui respectent ce standard. Fish est le seul shell de cette liste qui n'est pas compatible POSIX.

Ces shells sont disponibles sur de nombreux systèmes d'exploitation, comme GNU/Linux, FreeBSD, OpenBSD, macOS et dans une certaine mesure Windows avec WSL. Comprenons ainsi qu'une telle application compatible pourra être exécutée sur toutes les plateformes, sans adaptation ou presque.

Il faut maintenant parler des performances de ces différents shells. Bien entendu, ayant à faire à un langage interprété, les performances ne seront pas au niveau d'un langage compilé. En revanche, tous les shells n'offrent pas la même réactivité. Sur les systèmes basés sur Debian, deux shells sont installés par défaut : bash pour la partie interactive dans un terminal, dash pour l'exécution des scripts. Dash, Debian Almquist Shell, est un shell qui n'intègre que les fonctionnalités liées à la norme POSIX et permet donc d'avoir de meilleures performances qu'avec bash. Ce gain en performance se paye par une souplesse moindre, c'est donc pour cela que le choix de conserver bash comme shell interactif a été fait.

## Scripts et norme POSIX

Nous avons donc deux bonnes raisons de privilégier les scripts POSIX :

- avec un système GNU/Linux de type Debian, la performance sera meilleure qu'avec un script bash, non compatible POSIX ;
- il sera distribuable et exécutable sur toute machine possédant un shell compatible.

Un script POSIX commence par la ligne suivante :

    #!/bin/sh

Cette ligne indique au système d'exploitation le logiciel avec lequel le script doit être exécuté. Ici, le logiciel sera donc `/bin/sh` qui est généralement un simple lien vers un autre shell, dash sur un système Debian, bash sur un système Arch Linux. À cette commande sont parfois ajoutés quelques arguments qui permettent en particulier de contrôler le comportement du script en cas d'erreur.

Lors de la rédaction d'un script compatible POSIX, il faudra dont prendre soin de n'utiliser que des écritures compatibles. Cela peut être vérifié avec le programme `shellcheck` qui parcourt l'ensemble du script à la recherche de syntaxes non optimales ou non compatibles POSIX dès lors que la ligne indiquée ci-dessus apparait sur la première ligne de code du script. On pourra se référer à la page [GitHub](https://github.com/koalaman/shellcheck) du projet pour plus d'informations.

## La recherche de performance dans les scripts

Rechercher la meilleure performance possible dans un script shell peut paraitre vain. Pour d'autres, ce point aura une grande importance. Cela dépend de l'utilisation qui sera faite du script. Si ce script est utilisé une fois par jour sur demande de l'utilisateur, cette quête de la performance maximale n'en vaut pas la peine. En revanche, si c'est un script lié à une surveillance d'un système informatique qui est exécuté automatiquement par le système plusieurs fois par seconde, cette recherche de performance est importante.

Pour atteindre cet objectif, il est possible d'appliquer les quelques règles suivantes :

- utiliser dash comme shell exécutant les scripts compatibles POSIX ;
- limiter les appels à des programmes non nécessaires ;
- limiter les dépendances des scripts ;
- utiliser au maximum les tuyaux (pipes en anglais) pour faire passer des informations d'un programme à un autre.

Toutefois, la compréhension des actions d'un programme lors de la lecture de son code pouvant être difficile même pour un utilisateur expérimenté, il faudra toujours privilégier une bonne lisibilité du code à une amélioration des performances du programme.

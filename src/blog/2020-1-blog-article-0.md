[title]: <> (Blog, article 0)
[date]: <> (2020-06-26)

# Blog, article 0

Bienvenue sur mon blog personnel. Qui dit blog personnel dit que les sujets abordés ici seront ceux qui me tiennent à cœur, ils sont multiples et seront choisis au moment de l’écriture de chaque billet de blog. Il y a de fortes de chances que vous y trouviez des articles sur les logiciels que j'utilise, la pratique du vélo en ville et éventuellement sur ma pratique de la cuisine. Sans constituer une liste exhaustive, celle-ci permet au moins de donner une première idée des sujets qui y seront abordés. Un ensemble de sujets ne sera a priori pas abordé ici, ceux en lien avec mon activité professionnelle, aussi passionnante soit-elle.

Ce blog aura aussi, du moins à terme, une partie rédigée en anglais, de manière à renforcer le côté international de celui-ci. Le plus gros frein à ce projet est aujourd’hui d’ordre technique.

Chaque article de blog sera rédigé avec [vim](https://www.vim.org/), et le code html est généré par [ssg5](https://www.romanzolotarev.com/ssg.html) et un processus d’intégration continue et de déploiement continu. Cela fera l’objet d’un article détaillé. En revanche, vous n'y trouverez pas de section commentaire. Ce n'est pas par volonté de museler l'auditoire, mais plutôt de garder ce site le plus simple possible à maintenir. En revanche, il sera toujours possible de me contacter par voie électronique, par mail, et je pourrais éventuellement faire un article réponse ou revenir sur un point particulier qui aura été abordé dans un tel courriel.

Il ne me reste plus qu’à vous souhaiter une bonne lecture !

[title]: <> (Raccourcis sur iOS)
[date]: <> (2020-10-30)

# Raccourcis sur iOS

En guise de suite à notre [précédent article](/blog/2020-18-le-scripting-comme-mode-de-vie.html), nous présentons ici l'application [Raccourcis](https://apps.apple.com/fr/app/raccourcis/id915249334) disponible sur iOS. Elle permet d'automatiser quelques actions sur un iPhone ou un iPad.

<img src="/img/7-raccourcis.png" alt="Application Raccourcis sur iOS" class="img-responsive">

## Le scripting sur un smartphone

Un smartphone est ni plus ni moins qu'un ordinateur que nous avons sur nous en permanence. Il peut être intéressant d'automatiser quelques actions qui dépendent du lieu où nous sommes, d'une certaine heure ou d'un contexte particulier.

L'étendue des actions réalisables est naturellement plus limité que sur un ordinateur, et le choix du langage de programmation est imposé. Dans le cas de l'application Raccourcis, c'est une interface graphique assez bien pensée qui rappelle quelques peu l'interface de [scratch](https://scratch.mit.edu/) qui sert de « langage » de programmation.

Nous pouvons présenter deux cas d'usage : la récupération et l'affichage des principaux paramètres météorologiques d'un lieu, et l'automatisation de cette tâche à un moment bien choisi de la journée.

## Récupération et affichage de la météo

Ce raccourci, librement [téléchargeable](https://www.icloud.com/shortcuts/1fce1e10e80b4e7e9d6c6881070bb074), permet de récupérer les paramètres météorologiques du lieu où se trouve actuellement le téléphone et les prévisions météorologiques du jour, comme le taux de précipitations et les températures maximale et minimale. Il affiche ensuite une notification permettant de fournir à l'utilisateur les informations souhaitées.

## Automatisation de ce raccourci

Cette tâche peut être effectuée chaque jour, chaque heure ou lors de votre départ d'un lieu prédéfini. Dans ce cas, il suffit d'indiquer dans l'onglet « Automatisation » que vous souhaitez que le raccourci s'exécute dans un contexte particulier. Un contexte intéressant est le matin en semaine lors de l'extinction de votre réveil, ou bien tous les jours à une heure choisie.

## Aller plus loin

Il est des cas où les options offertes par l'application Raccourcis ne sont pas suffisantes. Il existe fort heureusement un moyen relativement simple pour étendre les fonctionnalités de cette application : exécuter une commande sur un poste distant via ssh, avec en entrée un certain nombre d'arguments tirés du raccourci. Dans ce cas, il faut bien entendu avoir accès à un serveur, mais cela permet d'exécuter n'importe quel programme, celui-ci pouvant être écrit dans n'importe quel langage de programmation, ce qui nous libère du carcan imposé par Apple.

# Accueil

Bienvenue sur mon site personnel. Il contient :

- un [blog](/blog) ;
- des [scripts](/bin).

et d'autres contenus qui restent à définir. L'objectif premier est de présenter des technologies qui me semblent intéressantes et qui peuvent en intéresser d'autres, en français, avec un niveau d'exigence concernant la qualité des informations exposées.

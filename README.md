# www.dioptre.fr sources

A personal static website of a random guy with interest in a lot of random things. It can be found at [www.dioptre.fr](https://www.dioptre.fr). The generator used here is [ssg5](https://www.romanzolotarev.com/ssg.html) made by Roman Zolotarev.
